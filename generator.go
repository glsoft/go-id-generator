package idgenerator

import (
	"database/sql"
)

type IdGenerator interface {
	Init() error
	Next() (int64, error)
	SetCurrentId(id int64)
	GetCurrentId() int64
	SetBatchMax(b int64)
	GetBatchMax() int64
}

func NewIdGenerator(db *sql.DB, section string) (IdGenerator, error) {
	return newMySQLIdGenerator(db, section)
}
