package main

import (
	generator "gitee.com/glsoft/go-id-generator"
	log "github.com/sirupsen/logrus"
)

func main() {

	datasource := "root:root@tcp(localhost:3306)/test?charset=utf8"
	s, e := generator.NewService(datasource, ":5678",true)
	if e != nil {
		log.Error(e)
		panic(e)
	}
	_ = s.Start()

}
