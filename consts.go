package idgenerator

const (
	BatchCount = 1000 // 每次取1000个

	KeyRecordTableName = "id_generator"

	CreateRecordTableNTSQLFormat = `CREATE TABLE IF NOT EXISTS %s (k VARCHAR(32) NOT NULL,id bigint(20) unsigned NOT NULL,PRIMARY KEY (k)) ENGINE=Innodb DEFAULT CHARSET=utf8 `

	InsertKeySQLFormat  = "INSERT INTO %s (k,id) VALUES ('%s',%d)"
	SelectForUpdate     = "SELECT id FROM %s WHERE k = '%s' FOR UPDATE"
	SelectKeySQLFormat  = "SELECT k FROM %s WHERE k = '%s' FOR UPDATE"
	SelectKeysSQLFormat = "SELECT k FROM %s"
	UpdateKeySQLFormat  = "UPDATE %s SET id = id + %d WHERE k = '%s'"
	DeleteKeySQLFormat  = "DELETE FROM %s WHERE k = '%s'"

)
