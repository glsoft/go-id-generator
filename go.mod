module gitee.com/glsoft/go-id-generator

go 1.16

require (
	github.com/go-redis/redis/v8 v8.11.4
	github.com/go-sql-driver/mysql v1.6.0
	github.com/pingcap/errors v0.11.4
	github.com/sirupsen/logrus v1.8.1
)
