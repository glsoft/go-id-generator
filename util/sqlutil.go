package util

import "database/sql"

func CloseRows(rows *sql.Rows) {
	_ = rows.Close()
}
