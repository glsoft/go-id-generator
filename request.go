package idgenerator

import (
	"bufio"
	"fmt"
	"github.com/pingcap/errors"
	"io"
	"io/ioutil"
	"net"
	"strconv"
	"strings"
)

var (
	ErrMethodNotSupported = &ErrorReply{"Method is not supported"}
	ErrNotEnoughArgs      = &ErrorReply{"Not enough arguments for the command"}
	ErrExpectInteger      = &ErrorReply{"Expected integer"}
	ErrExpectAuth         = &ErrorReply{"password invalid"}
	ErrExpectEvenPair     = &ErrorReply{"Got uneven number of key val pairs"}

	// ErrTooMuchArgs          = &ErrorReply{"Too many arguments for the command"}
	// ErrWrongArgsNumber      = &ErrorReply{"Wrong number of arguments"}
	// ErrExpectPositiveInteger = &ErrorReply{"Expected positive integer"}
	// ErrExpectMorePair       = &ErrorReply{"Expected at least one key val pair"}
	// ErrExpectEvenPair       = &ErrorReply{"Got uneven number of key val pairs"}

	ErrNoKey = &ErrorReply{"no key for set"}
)

type Request struct {
	Command       string
	Arguments     [][]byte
	RemoteAddress string
	Connection    io.ReadCloser
}

func (r *Request) HasArgument(index int) bool {
	return index >= 0 && index < len(r.Arguments)
}

func (r *Request) ExpectArgument(index int) *ErrorReply {
	if !r.HasArgument(index) {
		return ErrNotEnoughArgs
	}
	return nil
}

func (r *Request) GetInt(index int) (int64, *ErrorReply) {
	if errReply := r.ExpectArgument(index); errReply != nil {
		return -1, errReply
	}
	if n, err := strconv.ParseInt(string(r.Arguments[index]), 10, 64); err != nil {
		return -1, ErrExpectInteger
	} else {
		return n, nil
	}
}

func NewRequest(conn net.Conn) (*Request, error) {
	reader := bufio.NewReader(conn)
	// *<number of arguments>CRLF
	line, err := reader.ReadString('\n')
	if err != nil {
		return nil, err
	}

	var argCount int
	if line[0] == '*' {
		if _, err := fmt.Sscanf(line, "*%d\r\n", &argCount); err != nil {
			return nil, Malformed("*<#Arguments>", line)
		}

		// $<number of bytes of argument 1>CRLF
		// <argument data>CRLF
		command, err := readArgument(reader)
		if err != nil {
			return nil, err
		}

		arguments := make([][]byte, argCount-1)
		for i := 0; i < argCount-1; i++ {
			if arguments[i], err = readArgument(reader); err != nil {
				return nil, err
			}
		}

		return &Request{
			Command:    strings.ToUpper(string(command)),
			Arguments:  arguments,
			Connection: conn,
		}, nil
	}

	return nil, fmt.Errorf("new request error")
}

func readArgument(reader *bufio.Reader) ([]byte, error) {
	line, err := reader.ReadString('\n')
	if err != nil {
		return nil, Malformed("$<ArgumentLength>", line)
	}

	var argLength int
	if _, err := fmt.Sscanf(line, "$%d\r\n", &argLength); err != nil {
		return nil, Malformed("$<ArgumentLength>", line)
	}

	data, err := ioutil.ReadAll(io.LimitReader(reader, int64(argLength)))
	if err != nil {
		return nil, err
	}
	if len(data) != argLength {
		return nil, MalformedLength(argLength, len(data))
	}
	if b, err := reader.ReadByte(); err != nil || b != '\r' {
		return nil, MalformedMissingCRLF()
	}
	if b, err := reader.ReadByte(); err != nil || b != '\n' {
		return nil, MalformedMissingCRLF()
	}

	return data, nil
}

func Malformed(expected string, got string) error {
	return errors.Errorf("Malformed request: %s does not match %s", got, expected)
}

func MalformedLength(expected int, got int) error {
	return errors.Errorf("Malformed request: argument length %d does not match %d", got, expected)
}

func MalformedMissingCRLF() error {
	return errors.Errorf("Malformed request: line should end with CRLF")
}
